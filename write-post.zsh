#!/usr/bin/env zsh
zmodload zsh/datetime
integer start
integer -Z2 day
strftime -r -s start %Y-%m-%d 2020-01-02
((day = (EPOCHSECONDS - start)/(3600 * 24)))
post=post/100-days-$day.md

cd $HOME/Repos/xpmo.gitlab.io
if ! [[ -f content/$post ]]; then
	hugo new $post
	sed -i "2ctitle: \"100 Days of Code: Day $day\"" content/$post
fi
$VISUAL content/$post
