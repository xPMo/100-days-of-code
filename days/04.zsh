#!/usr/bin/env zsh
local -a output {,_}{alphas,betas} switch ackchk

# read from arguments
# ":"    indicates a mandatory argumen
# "+"    indicates to append to the array instead of overwrite
# "=foo" indicates to use array $foo for this option

zparseopts -switch:=switch s:=switch y=ackchk \
  o:=output -output:=output \
  -alpha+:=_alphas a+:=_alphas \
  -beta+:=_betas b+:=_betas

# remove flags from alpha, beta arrays
for flag val in "${(@)_alphas}"; do
  alphas+=("$val")
done
for flag val in "${(@)_betas}"; do
  betas+=("$val")
done

# use the (qq) flag to single-quote everything

echo "Alpha arguments: ${(@qq)alphas}"
echo "Beta  arguments: ${(@qq)betas}"

if (( $#ackchk )); then
  echo "Acknowledge check!"
fi

if (( $#output )); then
  echo "Output: ${(qq)output[2]}"
fi

if (( $#switch )); then
  case $switch[2] in
    x) echo "Swtich case: x" ;;
    y) echo "Switch case: y" ;;
    z) echo "Switch case: z" ;;
    *) echo "BAD SWITCH: ${(qq)switch[2]}" ;;
  esac
fi

echo
